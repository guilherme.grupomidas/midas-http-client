# GRUPO MIDAS - HTTP CLIENT

Cliente HTTP para realizar requisições para APIs REST com dados em JSON. Todos os métodos HTTP devem ser executados na forma de Promises, conforme exemplos abaixo.

## Requisitos
Necessário jQuery já importado.

## API

### Instanciando a classe
Possíveis construtures:
 * MidasHttpClient()
 * MidasHttpClient(url_da_api)
 * MidasHttpClient(url_da_api, timeout_em_milissegundos)

```javascript
 var httpClient = new MidasHttpClient('http://localhost/api', 10000)
```

### Configurando o client
Métodos disponíveis:
 * setUrlApi(url_da_api)
 * setTimeout(timeout_em_milissegundos)
 * setApiToken(token) ou setApiToken(token, campo_do_header)
 * setDebug(booleano)

```javascript
 var httpClient = new MidasHttpClient()
 // define a url antes de realizar o GET
 htpClient.setUrlApi('http://localhost/api').get()
 // define o TIMEOUT antes de realizar o GET
 htpClient.setTimeout(3000).get()
 // define um TOKEN, o HEADER do token e o TIMEOUT antes de realizar o GET
 htpClient.setApiToken('asdofuhasdfohasdofuh23421', 'x-access-token').setTimeout(3000).get()
 // habilita debug de erros
 htpClient.setDebug(true)
```

### GET
O método GET deve ter como primeiro parâmetro o ponto da API para busca de dados. A execução retorna uma Promise com os dados ou um objeto de erro.

```javascript
  httpClient.get('/usuarios')
    .then(res => {
      console.log('SUCESSO NO GET')
      console.log(res) // RETORNO EM JSON
    }).catch(err => {
      console.log('ERRO NO GET')
      console.log(err)
    })
```

### DELETE
O método DELETE deve ter como primeiro parâmetro o ponto da API para busca de dados. A execução retorna uma Promise com os dados ou um objeto de erro.

```javascript
  httpClient.delete('/usuarios/2')
    .then(res => {
      console.log('SUCESSO NO DELETE')
      console.log(res) // RETORNO EM JSON
    }).catch(err => {
      console.log('ERRO NO DELETE')
      console.log(err)
    })
```

### POST
O método POST deve ter como primeiro parâmetro o ponto da API para busca de dados. O segundo parâmetro deve ser um objeto com os dados a serem enviados. A execução retorna uma Promise com os dados ou um objeto de erro.

```javascript
  httpClient.post('/usuarios', {})
    .then((res) => {
      console.log('SUCESSO NO POST')
      console.log(res) // RETORNO EM JSON OU UNDEFINED EM CASO DE RETORNO 204
    }).catch(err => {
      console.log('ERRO NO POST')
      console.log(err)
    })
```

### PUT
O método PUT deve ter como primeiro parâmetro o ponto da API para busca de dados. O segundo parâmetro deve ser um objeto com os dados a serem enviados. A execução retorna uma Promise com os dados ou um objeto de erro.

```javascript
  httpClient.put('/usuarios/2', {})
    .then(res => {
      console.log('SUCESSO NO PUT')
      console.log(res) // RETORNO EM JSON
    }).catch(err => {
      console.log('ERRO NO PUT')
      console.log(err)
    })
```