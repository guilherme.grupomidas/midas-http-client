var MidasHttpClient = function (urlApi, timeout) {
  if (typeof urlApi === 'string' && urlApi.length > 0 && (!/^http:\/\//.test(urlApi) && !/^https:\/\//.test(urlApi))) {
    throw new Error('A URL da API deve ser informada como uma string iniciada por http:// ou https://.')
  }
  if (typeof timeout !== 'undefined' && isNaN(timeout)) {
    throw new Error('O timeout deve ser informado como um número inteiro positivo (milissegundos).')
  }
  this.debug = false
  this.urlApi = urlApi.replace(/\/$/, '') || null
  this.timeout = timeout || 10000
  this.contentType = 'application/json'
  this.token = {
    campoHeader: null,
    token: null
  }
}

MidasHttpClient.prototype.setUrlApi = function (urlApi) {
  this.urlApi = urlApi
  return this
}

MidasHttpClient.prototype.setTimeout = function (timeout) {
  if (typeof timeout !== 'undefined' && !isNaN(timeout)) {
    this.timeout = timeout
  }
  return this
}

MidasHttpClient.prototype.setApiToken = function (token, campoHeader) {
  this.token.campoHeader = campoHeader || 'Authorization'
  this.token.token = token
  return this
}

MidasHttpClient.prototype.setDebug = function (debug) {
  this.debug = (debug === true || debug === 'true')
  return this
}

MidasHttpClient.prototype.get = function (sufixo) {
  var p = new Promise((resolve, reject) => {
    sufixo = sufixo.replace(/^\//, '')
    var url = `${this.urlApi}/${sufixo}`
    var configuracaoAjax = {
      url: url,
      method: 'GET',
      timeout: this.timeout,
      success: (data, textStatus, jqXHR) => {
        if (typeof data === 'object') {
          return resolve(data)
        } else {
          try {
            var jsonData = JSON.parse(data)
            return resolve(jsonData)
          } catch (e) {
            console.log(e)
            return reject(new Error('A resposta da requisição não retornou um objeto JSON válido.'))
          }
        }
      },
      error: (jqXHR, textStatus, errorThrown) => {
        if (this.debug) {
          console.log('- ERRO AO REALIZAR UMA REQUISIÇÃO - GET - NO HTTP CLIENT')
          console.log('jqXHR', jqXHR)
          console.log('textStatus', textStatus)
          console.log('errorThrown', errorThrown)
        }
        this.codigosErrosConhecidos.map((erro) => {
          if (erro.codigo === jqXHR.status) return reject(new Error(erro.mensagem))
        })
        return reject(new Error(errorThrown))
      }
    }
    if (this.token.campoHeader && this.token.token) {
      configuracaoAjax.beforeSend = (request) => {
        request.setRequestHeader(this.token.campoHeader, this.token.token)
      }
    }
    $.ajax(configuracaoAjax)
  })
  return p
}

MidasHttpClient.prototype.delete = function (sufixo) {
  var p = new Promise((resolve, reject) => {
    sufixo = sufixo.replace(/^\//, '')
    var url = `${this.urlApi}/${sufixo}`
    var configuracaoAjax = {
      url: url,
      method: 'DELETE',
      timeout: this.timeout,
      success: (data, textStatus, jqXHR) => {
        if (typeof data === 'object') {
          return resolve(data)
        } else {
          if (data) {
            try {
              var jsonData = JSON.parse(data)
              return resolve(jsonData)
            } catch (e) {
              console.log(e)
              return reject(new Error('A resposta da requisição não retornou um objeto JSON válido.'))
            }
          } else {
            return resolve()
          }
        }
      },
      error: (jqXHR, textStatus, errorThrown) => {
        if (this.debug) {
          console.log('- ERRO AO REALIZAR UMA REQUISIÇÃO - DELETE - NO HTTP CLIENT')
          console.log('jqXHR', jqXHR)
          console.log('textStatus', textStatus)
          console.log('errorThrown', errorThrown)
        }
        this.codigosErrosConhecidos.map((erro) => {
          if (erro.codigo === jqXHR.status) return reject(new Error(erro.mensagem))
        })
        return reject(new Error(errorThrown))
      }
    }
    if (this.token.campoHeader && this.token.token) {
      configuracaoAjax.beforeSend = (request) => {
        request.setRequestHeader(this.token.campoHeader, this.token.token)
      }
    }
    $.ajax(configuracaoAjax)
  })
  return p
}

MidasHttpClient.prototype.post = function (sufixo, dados) {
  var p = new Promise((resolve, reject) => {
    sufixo = sufixo.replace(/^\//, '')
    var url = `${this.urlApi}/${sufixo}`
    var configuracaoAjax = {
      url: url,
      method: 'POST',
      contentType: this.contentType,
      dataType: 'json',
      timeout: this.timeout,
      data: JSON.stringify(dados),
      success: (data, textStatus, jqXHR) => {
        if (typeof data === 'object') {
          return resolve(data)
        } else {
          try {
            var jsonData = JSON.parse(data)
            return resolve(jsonData)
          } catch (e) {
            console.log(e)
            return reject(new Error('A resposta da requisição não retornou um objeto JSON válido.'))
          }
        }
      },
      error: (jqXHR, textStatus, errorThrown) => {
        if (this.debug) {
          console.log('- ERRO AO REALIZAR UMA REQUISIÇÃO - POST - NO HTTP CLIENT')
          console.log('jqXHR', jqXHR)
          console.log('textStatus', textStatus)
          console.log('errorThrown', errorThrown)
        }
        this.codigosErrosConhecidos.map((erro) => {
          if (erro.codigo === jqXHR.status) return reject(new Error(erro.mensagem))
        })
        return reject(new Error(errorThrown))
      }
    }
    if (this.token.campoHeader && this.token.token) {
      configuracaoAjax.beforeSend = (request) => {
        request.setRequestHeader(this.token.campoHeader, this.token.token)
      }
    }
    $.ajax(configuracaoAjax)
  })
  return p
}

MidasHttpClient.prototype.put = function (sufixo, dados) {
  var p = new Promise((resolve, reject) => {
    sufixo = sufixo.replace(/^\//, '')
    var url = `${this.urlApi}/${sufixo}`
    var configuracaoAjax = {
      url: url,
      method: 'PUT',
      contentType: this.contentType,
      dataType: 'json',
      timeout: this.timeout,
      data: JSON.stringify(dados),
      success: (data, textStatus, jqXHR) => {
        if (typeof data === 'object') {
          return resolve(data)
        } else {
          try {
            var jsonData = JSON.parse(data)
            return resolve(jsonData)
          } catch (e) {
            console.log(e)
            return reject(new Error('A resposta da requisição não retornou um objeto JSON válido.'))
          }
        }
      },
      error: (jqXHR, textStatus, errorThrown) => {
        if (this.debug) {
          console.log('- ERRO AO REALIZAR UMA REQUISIÇÃO - PUT - NO HTTP CLIENT')
          console.log('jqXHR', jqXHR)
          console.log('textStatus', textStatus)
          console.log('errorThrown', errorThrown)
        }
        this.codigosErrosConhecidos.map((erro) => {
          if (erro.codigo === jqXHR.status) return reject(new Error(erro.mensagem))
        })
        return reject(new Error(errorThrown))
      }
    }
    if (this.token.campoHeader && this.token.token) {
      configuracaoAjax.beforeSend = (request) => {
        request.setRequestHeader(this.token.campoHeader, this.token.token)
      }
    }
    $.ajax(configuracaoAjax)
  })
  return p
}

MidasHttpClient.prototype.codigosErrosConhecidos = [
  {codigo: 0, mensagem: 'Servico indisponivel.'},
  {codigo: 401, mensagem: 'Autenticação necessária.'},
  {codigo: 403, mensagem: 'Acesso negado.'},
  {codigo: 404, mensagem: 'Erro 404. Não encontrado.'}
]
